package com.example.file_reciever.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileReceiveController {

    @PostMapping("/accept-files")
    public HttpStatus acceptFiles(@RequestBody String data) {
        System.out.println("Accepted: " + data);
        return HttpStatus.ACCEPTED;
    }
}
