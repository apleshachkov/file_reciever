package com.example.file_reciever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileRecieverApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileRecieverApplication.class, args);
    }

}
