# file_receiver
JDK 17, Spring Boot.
Порт по дефолту - 8081.
Отвечает за получение файлов по ресту и вывод содержимого файла на консоль.

# Реализуемое API:
- /accept-files	Получение файла через POST-запрос

# Входящие параметры:
- data	файл в формате строки

# Возвращаемые параметры:
Код статуса ответа: 202 ACCEPTED в любом случае, кроме внутренней ошибки приложения; 


# Параметры локального запуска:
- Spring Boot Application -> run
- cmd -> java -jar path_to_source\file_reciever-0.0.1-SNAPSHOT.jar

![img.png](img.png)